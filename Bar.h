#pragma once
#include <SFML\Graphics.hpp>
#include "Config.h"

class Bar : public sf::RectangleShape
{
public:
	Bar() = delete;
	Bar(float x, float y, const float& width, const float& height);
	~Bar() override;
};

struct Paddle : public sf::RectangleShape {
	static const int vert = 175;
	static const int hor = 50;
	static const int offset_x = 60;
	static const int offset_y = 2;
	static const int offset_x_tb = 3;
	static const int offset_y_tb = 180;

	enum SIDE {
		TOP = 0,
		RIGHT = 1,
		BOTTOM = 2,
		LEFT = 3,
		NUMBER
	};
	Paddle(float _x, float _y) : x(_x), y(_y)
	{
		boxes[SIDE::LEFT].setSize(sf::Vector2f(1.f, static_cast<float>(vert)));
		boxes[SIDE::LEFT].setPosition(x - 5, y);
		boxes[SIDE::RIGHT].setSize(sf::Vector2f(1, static_cast<float>(vert)));
		boxes[SIDE::RIGHT].setPosition(x + offset_x, y);
		boxes[SIDE::TOP].setSize(sf::Vector2f(static_cast<float>(hor), 1));
		boxes[SIDE::TOP].setPosition(x + offset_x_tb, y - offset_y);
		boxes[SIDE::BOTTOM].setSize(sf::Vector2f(static_cast<float>(hor), 1));
		boxes[SIDE::BOTTOM].setPosition(x + offset_x_tb, y + 180);

		rs.setPosition(x, y);
		rs.setFillColor(sf::Color::Blue);
		rs.setSize(sf::Vector2f(gamebar_width, gamebar_height));
	}
	
	Paddle(float _x, float _y, char _type) : x(_x), y(_y), type(_type)
	{
		rs.setPosition(x ,y);
		rs.setFillColor(sf::Color::Green);
		rs.setSize(sf::Vector2f(score_bar_width, score_bar_height));
	}

	void move(float dx, float dy); 

	sf::RectangleShape boxes[4];
	float x, y;
	sf::RectangleShape rs;
	char type;
};