#pragma once
#include <SFML\Graphics.hpp>

class UItext : public sf::Text
{
public:
	UItext(const std::string& text, sf::Font& font);
	UItext(int& text, sf::Font& font, const float& x, const float& y);
};