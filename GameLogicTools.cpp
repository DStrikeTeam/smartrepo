#include "stdafx.h"
#include "GameLogicTools.h"
#include <windows.h>


float GameLogicTools::rangeRandomAlg2(int min, int max)
{
	auto r = rand() % 2;
	return (r == 0) ? min : max;
}