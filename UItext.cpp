#include "stdafx.h"
#include "UItext.h"
#include "Config.h"

UItext::UItext(const std::string& text, sf::Font& font)
{
	setString(text);
	setFont(font);
	setFillColor(sf::Color::Red);
	setPosition(default_position_X, default_position_Y);
	setCharacterSize(50);
}

UItext::UItext(int& text, sf::Font& font, const float& x, const float& y)
{
	setString(std::to_string(text));
	setFont(font);
	setFillColor(sf::Color::Red);
	setPosition(x, y);
	setCharacterSize(50);
}