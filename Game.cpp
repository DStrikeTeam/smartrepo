#include "stdafx.h"

#include <SFML\window.hpp>
#include <SFML\Graphics.hpp>

#include "Game.h"
#include "Ball.h"
#include "Bar.h"
#include "Config.h"
#include "UItext.h"
#include "Physics.h"
#include "GameLogicTools.h"
#include "InputSystem.h"

#include <iostream>
#include <windows.h>


void Game::run() {
	sf::RenderWindow window(sf::VideoMode(gameWindowWidth, gameWindowHeight), "SmartPONG");
	sf::Font font;
	if (!font.loadFromFile("FontFile.ttf"))
	{
		return;
	}

	sf::Clock clock;
	
	sf::Vector2f direction(GLT::rangeRandomAlg2(13, -13), GLT::rangeRandomAlg2(9,-9));
	const float velocity = std::sqrt(direction.x * direction.x + direction.y * direction.y);

	sf::Time elapsed = clock.restart();
	const sf::Time update_ms = sf::seconds(1.f / 30.f);
	//update random engine, timer

	UItext text_continue("Press R to restart or Q to quit", font);

	//Bar enemy_bar(gamebar_x + 558, gamebar_y, gamebar_width, gamebar_height); //right paddle
	
	Bar score_bar(score_bar_x, score_bar_y, score_bar_width, score_bar_height); //left_gate
	Bar score_bar_opfor(score_bar_x_opfor, score_bar_y, score_bar_width, score_bar_height); //right_gate

	//////////test

	Paddle padtest(gamebar_x, gamebar_y);
	Paddle padtest2(enemy_gamebar_x, gamebar_y);

	////////////////
	sf::Clock AITimer;
	const sf::Time AITime = sf::seconds(0.1f);
	float opfor_barspeed = 0.f;
	////////////////

	Ball ball(default_position_X, default_position_Y, gameball_radius);
	ball.setOrigin(ball.getRadius()*0.5f, ball.getRadius()*0.5f);

	int score_left = 0;
	int score_right = 0;

	UItext playerscore(score_left, font, 4, 1);
	UItext aiscore(score_right, font, 775, 1);
	
	bool isPlaying = true;
	bool isScoring = false;

	std::vector<sf::RectangleShape*> vec = { &score_bar, &score_bar_opfor };

	sf::Event event;
	while (window.isOpen())
	{
		if (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				continue;

			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape)
				{
					window.close();
					continue;
				}
				break;
			default:
				break;
			}
		}

		
		elapsed += clock.restart();
		while (elapsed >= update_ms) {

			if (isPlaying) {

				const auto pos = ball.getPosition();
				const auto delta = update_ms.asSeconds() * velocity;

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && (padtest.boxes[0].getPosition().y > 1))

				{
					padtest.move(dx, -dy);
				}

				//InputSystem::MoveUP(sf::Keyboard::Up, box_top, box_bottom, box_left, box_right, bar, gamewindow_min, screenHeight, dx, dy);

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && (padtest.boxes[2].getPosition().y < 600))

				{
					padtest.move(dx, dy);
				}
				
				sf::Vector2f new_pos(pos.x + direction.x * delta, pos.y + direction.y * delta);

				Phys::CollidesWithScreen(direction, new_pos, gameball_radius, screen_min, screenHeight, screenWidth);
				
				//Phys::BallIntersectsBar(ball, new_pos, direction, box_left, box_right, box_top, box_bottom, gameball_radius);

				//Phys::BallIntersectsBar(ball, new_pos, direction, box_left_opfor, box_right_opfor, box_bottom_opfor, box_top_opfor, gameball_radius);
				
				if (ball.getGlobalBounds().intersects(score_bar.getGlobalBounds())) 
				{
					score_right++;
					playerscore.setString(std::to_string(score_right));
					isScoring = true;
					if (score_left == 3)
						isPlaying = false;
				}

				if (ball.getGlobalBounds().intersects(score_bar_opfor.getGlobalBounds()))
				{
					score_left++;
					aiscore.setString(std::to_string(score_left));
					isScoring = true;
					if (score_right == 3)
						isPlaying = false;
				}

				if (((opfor_barspeed < 0.f) && (padtest2.rs.getPosition().y < screenHeight) || ((opfor_barspeed >= 0.f) && (padtest2.rs.getPosition().y > 1))))
				{
					padtest2.move(dx, opfor_barspeed);
				}

				if (AITimer.getElapsedTime() > AITime) 
				{
					AITimer.restart();
					if (ball.getPosition().y - gameball_radius > padtest2.rs.getPosition().y)
						opfor_barspeed = dy;
					else if (ball.getPosition().y + gameball_radius < padtest2.rs.getPosition().y + gamebar_height)
						opfor_barspeed = -dy;
					else
						opfor_barspeed = dx;
				}

				if (padtest2.rs.getPosition().y < 2)
				{
					padtest2.setPosition(gamebar_x + 558, 6.f);
				}

				if (padtest2.getPosition().y >= 425)
				{
					padtest2.setPosition(gamebar_x + 558, 420.f);
				}


				ball.setPosition(new_pos);
				elapsed -= update_ms;
			}

			if (isPlaying)
			{
				window.clear();
				for (auto & i : vec)
				{
					window.draw(*i);
				}
				window.draw(padtest.boxes[0]);
				window.draw(padtest.boxes[1]);
				window.draw(padtest.boxes[2]);
				window.draw(padtest.boxes[3]);
				window.draw(padtest.rs);
				window.draw(padtest2.rs);
				window.draw(ball);
				window.draw(playerscore);
				window.draw(aiscore);
			}

			if (isScoring)
			{
				window.clear();
				ball.setPosition(default_position_X, default_position_Y);
				//bar.setPosition(gamebar_x - 5, gamebar_y);
				padtest2.setPosition(gamebar_x + 558, gamebar_y);
				window.display();
				isScoring = false;
			}

			if (!isPlaying)
			{
				window.clear();
				window.draw(text_continue);
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				{
					isPlaying = true;
					clock.restart();
					score_left = 0;
					score_right = 0;
					ball.setPosition(default_position_X, default_position_Y);
					//bar.setPosition(gamebar_x - 5, gamebar_y);
					padtest2.setPosition(gamebar_x + 558, gamebar_y);
					Sleep(100);
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
				{
					return;
				}
			}
			window.display();
		}
	}
}