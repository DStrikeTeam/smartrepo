#pragma once
#include <SFML\Graphics.hpp>
#include "Ball.h"

namespace Physics {
	template <typename T>
	void reverseToRight(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset);
	template <typename T>
	void reverseToLeft(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset);
	template <typename T>
	void reverseToBottom(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset);
	template <typename T>
	void reverseToTop(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset);

	void reverseToBottomX(sf::Vector2f& direction, sf::Vector2f& pos, const int& border, const float& offset);
	void CollidesWithScreen(sf::Vector2f& direction, sf::Vector2f& pos, float gb_radius, const float& min_border, const float& max_border_height, const float& max_border_width);
	void BallIntersectsBar(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction, sf::RectangleShape& box_left, sf::RectangleShape& box_right, sf::RectangleShape& box_top, sf::RectangleShape& box_bottom, float gameball_radius);
	
	template <typename T>
	bool collideL(sf::Vector2f& a, float b, T);
	template <typename T>
	bool collideR(sf::Vector2f& a, float b, T);
	template <typename T>
	bool collideT(sf::Vector2f& a, float b, T);
	template <typename T>
	bool collideB(sf::Vector2f& a, float b, T);
}

namespace Phys = Physics;


