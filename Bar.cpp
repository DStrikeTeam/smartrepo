#include "stdafx.h"
#include "Bar.h"

Bar::Bar(float x, float y, const float& width, const float& height)
{
	RectangleShape(sf::Vector2f(width, height));
	setPosition(sf::Vector2f(x, y));
	setSize(sf::Vector2f(width, height));
	setFillColor(sf::Color(100, 200, 100));
}

void Paddle::move(float dx, float dy)
{
	for (auto& box : boxes)
		box.move(dx, dy);
	rs.move(dx, dy);
}

Bar::~Bar()
{
}
