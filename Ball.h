#pragma once


class Ball : public sf::CircleShape
{
public:
	Ball(float x, float y, const float& radius);

private:
	float m_speed;
};

