#include "stdafx.h"
#include <SFML\Graphics.hpp>
#include "Ball.h"


Ball::Ball(float x, float y, const float& radius) : 
	CircleShape(radius)
{
	setPosition(sf::Vector2f(x, y));
	setFillColor(sf::Color(200, 100, 100));
}
