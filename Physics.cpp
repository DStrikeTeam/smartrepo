#include "stdafx.h"
#include "Physics.h"

template <typename T>
void Phys::reverseToRight(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset)
{
	pos.x = border + offset;
	direction.x *= -1;
}

template <typename T>
void Phys::reverseToLeft(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset)
{
	pos.x = border - offset;
	direction.x *= -1;
}

template <typename T>
void Phys::reverseToBottom(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset)
{
	pos.y = border + offset;
	direction.y *= -1;
}

template <typename T>
void Phys::reverseToTop(sf::Vector2f& direction, sf::Vector2f& pos, const T& border, const float& offset)
{
	pos.y = border - offset;
	direction.y *= -1;
}

void Phys::CollidesWithScreen(sf::Vector2f& direction, sf::Vector2f& pos, float gb_radius, const float& min_border, const float& max_border_height, const float& max_border_width)
{
	if (Phys::collideL(pos, gb_radius, min_border))
	{
		Phys::reverseToRight(direction, pos, min_border, gb_radius);
	}
	if (Phys::collideR(pos, gb_radius, max_border_width))
	{
		Phys::reverseToLeft(direction, pos, max_border_width, gb_radius);
	}
	if (Phys::collideT(pos, gb_radius, min_border))
	{
		Phys::reverseToBottom(direction, pos, min_border, gb_radius);
	}
	if (Phys::collideB(pos, gb_radius, max_border_height))
	{
		Phys::reverseToTop(direction, pos, max_border_height, gb_radius);
	}
}

void Phys::BallIntersectsBar(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction, sf::RectangleShape& box_left, sf::RectangleShape& box_right, sf::RectangleShape& box_top, sf::RectangleShape& box_bottom, float gameball_radius)
{
	if (ball.getGlobalBounds().intersects(box_left.getGlobalBounds()))
	{
		new_pos = sf::Vector2f(ball.getPosition().x - gameball_radius - 2, ball.getPosition().y);
		direction.x *= -1;
	}

	if (ball.getGlobalBounds().intersects(box_right.getGlobalBounds()))
	{
		new_pos = sf::Vector2f(ball.getPosition().x + gameball_radius + 2, ball.getPosition().y);
		direction.x *= -1;
	}

	if (ball.getGlobalBounds().intersects(box_top.getGlobalBounds()))
	{
		new_pos = sf::Vector2f(ball.getPosition().x, ball.getPosition().y - gameball_radius - 2);
		direction.y *= -1;
	}

	if (ball.getGlobalBounds().intersects(box_bottom.getGlobalBounds()))
	{
		new_pos = sf::Vector2f(ball.getPosition().x, ball.getPosition().y + gameball_radius + 2);
		direction.y *= -1;
	}
}

template <typename T>
bool Phys::collideL(sf::Vector2f& a, float b, T c) { //from right to left
	return (a.x - b <= c) ? true : false;
}

template <typename T>
bool Phys::collideR(sf::Vector2f& a, float b, T c) { //from left to right
	return (a.x + b >= c) ? true : false;
}

template <typename T>
bool Phys::collideT(sf::Vector2f& a, float b, T c) { //from bottom to top
	return (a.y - b <= c) ? true : false;
}

template <typename T>
bool Phys::collideB(sf::Vector2f& a, float b, T c) { //from top to bottom
	return (a.y + b >= c) ? true : false;
}
