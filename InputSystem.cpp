#include "stdafx.h"
#include "InputSystem.h"


void InputSystem::MoveUP(sf::RectangleShape& box_top, sf::RectangleShape& box_bottom,
	sf::RectangleShape& box_left, sf::RectangleShape& box_right, Bar& bar, const float& dx, const float& dy)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && (box_top.getPosition().y > 1))
	{
		box_top.move(dx, -dy);
		box_bottom.move(dx, -dy);
		box_left.move(dx, -dy);
		box_right.move(dx, -dy);
		bar.move(dx, -dy);
	}
}