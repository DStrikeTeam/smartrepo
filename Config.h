#pragma once
#define _USE_MATH_DEFINES
#include <math.h>

//const float ballSpeed = 300.f;
const float pi = 3.14f;
const int gamewindow_min = 1;

const float screen_min = 0;
const unsigned int gameWindowHeight = 600;
const unsigned int gameWindowWidth = 800;
const float screenHeight = 600;
const float screenWidth = 800;

const float default_position_X = screenWidth * 0.5f;
const float default_position_Y = screenHeight * 0.5f;

//float gameball_y = (float) screenHeight * 0.5f;
const float gameball_radius = 15.f;

const float gamebar_x = 97.f;
const float gamebar_y = 250.f;
const float gamebar_width = 55.f;
const float gamebar_height = 175.f;

const float enemy_gamebar_x = 700.f;

const float score_bar_x = 20.f;
const float score_bar_y = 230.f;
const float score_bar_width = 20.f;
const float score_bar_height = 150.f;

const float score_bar_x_opfor = 780.f;

const float dx = 0.f;
const float dy = 5.f;

const float box_left_x = 200.f;
const float box_right_x = 250.f;
const float box_x = 203.f;

const float box_y = 250.f;
const float box_y_bottom = 425.f;