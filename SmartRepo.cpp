// SmartRepo.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include <SFML\Window.hpp>
#include "Game.h"


int WinMain()
{
	Game game;
	srand(time(NULL));
	game.run();

    return 0;
}

