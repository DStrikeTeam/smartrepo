#pragma once
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include "Bar.h"
#include "Config.h"


namespace InputSystem
{
	void MoveUP(sf::RectangleShape& box_top, sf::RectangleShape& box_bottom,
		sf::RectangleShape& box_left, sf::RectangleShape& box_right, Bar& bar, const float& dx, const float& dy);
}
